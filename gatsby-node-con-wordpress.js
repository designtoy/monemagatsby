const _ = require(`lodash`);
const Promise = require(`bluebird`);
const path = require(`path`);
const slash = require(`slash`);



const postsQuery = `
{
  allWordpressPost {
    edges {
      node {
        id,
        slug,
        content
      }
    }
  }
}
`


exports.createPages = ({ graphql, actions }) => {
    const { createPage } = actions;

    return new Promise((resolve, reject) => {

                graphql(postsQuery)
                    .then(result => {
                        if (result.errors) {
                            console.log(result.errors);
                            reject(result.errors);
                        }
                        const postTemplate = path.resolve("./src/pages/blogpost.jsx");
                        const postsTemplate = path.resolve("./src/pages/blog.jsx");

                        // Create Posts
                        createPage({
                            path: `/posts/`,
                            component: slash(postsTemplate)
                        });

                        _.each(result.data.allWordpressPost.edges, edge => {
                            createPage({
                                path: `/post/${edge.node.slug}/`,
                                component: slash(postTemplate),
                                context: {
                                    id: edge.node.id
                                },
                            });
                        });
                        resolve();
                    });
            });
        // ==== END POSTS ====
    }


