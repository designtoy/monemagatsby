import React from 'react'
import '../../assets/sass/aos.scss'
import '../../assets/sass/bootstrap.min.scss'
import '../../assets/sass/font-awesome.min.scss'
import './index.scss'

const Layout = ({children}) => {

  return (<div
        className="mainwrapper"
      >
        <div>
          <div>{children}</div>
        </div>
      </div>
    
    )}
    export default Layout


