import React from 'react'
import FontAwesome from 'react-fontawesome'

const Footer = () => {
  return (

      <footer id="footer" className="container-fluid">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="copyright">
                <p>
                  <span>Copyright</span> © 2018 | <span>monema.it</span>{' '}
                  P.IVA:IT02350530974
                </p>
                <p>Cookie Policy / Privacy Policy</p>
              </div>
            </div>
            <div className="col-lg-6">
            <div className="info">
            <FontAwesome
                    name="paper-plane"
                    size="2x"
                    className="icona"
                    style={{
                      color: 'white',
                      fontSize: '1.3rem',
                      margin:'1rem'
                    }}
                  /> 
              <a href="mailto:info@monema.it"> info@monema.it </a>  
              <FontAwesome
                    name="phone"
                    size="2x"
                    className="icona"
                    style={{
                      color: 'white',
                      fontSize: '1.3rem',
                      marginRight: '0.2rem',
                      marginLeft:'1rem'
                    }}
                  />  
                  +39 0574 870600     
                  <FontAwesome
                    name="clock-o"
                    size="2x"
                    className="icona"
                    style={{
                      color: 'white',
                      fontSize: '1.3rem',
                      marginRight: '0.2rem',
                      marginLeft:'1rem'
                    }}
                  /> 9-13 | 14-18
            </div>
            </div>
          </div>
        </div>
        
      </footer>

  )
}
export default Footer
