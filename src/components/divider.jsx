import React  from 'react'
import FontAwesome from 'react-fontawesome'

const Divider = () => (
  <div className="divider">
    <FontAwesome
      name="star"
      size="2x"
      className='divicona'
    />
  </div>
)
export default Divider
