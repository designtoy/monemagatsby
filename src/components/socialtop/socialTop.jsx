import React, { Component } from 'react'
import './socialTop.scss'
import { SocialIcon } from 'react-social-icons';
const style = {height: 30, width: 30, margin:'0.5rem', fill: 'gray'}
class SocialTop extends Component {
  render() {
    return (
    <div className="wrapper-social">
      <SocialIcon style={{...style}} url="https://www.facebook.com/monemaweb/" />
      <SocialIcon style={{...style}} url="https://twitter.com/monemaweb/" />
      <SocialIcon style={{...style}} url="https://www.linkedin.com/company/monemaweb" />
      </div>
    )
  }
}
export default SocialTop