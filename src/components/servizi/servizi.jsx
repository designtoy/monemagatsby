import React, { Component } from 'react'
import serviziImg from '../../assets/images/servizi.png'
class Servizi extends Component {
  render() {
    return (
   <section id='servizi' className='container-fluid' >
   <div className='container'>
       <div className="row">
       <div className="section-header">
        <h1>Servizi</h1>
        <h2>Possiamo esserti utili...</h2>
        <img src={serviziImg} alt=""/>
        </div>
       </div>
       <div className="row">
       <div className="col-lg-4">
       <div className="boxservizi">
       <h1>Ecommerce</h1>
       <p>Che tu stia ancora solo pensandoci o abbia già iniziato il tuo progetto eCommerce, noi siamo qui per aiutarti, dall’hosting dedicato, all’ottimizzazione o all’intero sviluppo della tua piattaforma di commercio elettronico.</p>
       </div>
       </div>
       <div className="col-lg-4">    <div className="boxservizi">
       <h1>WebDesign</h1>
       <p>Progettiamo e realizziamo siti web con cura e attenzione, mettendo in evidenza le peculiarità dell’azienda, la tua.</p>
       </div></div>
       <div className="col-lg-4">    <div className="boxservizi">
       <h1>Comunicazione</h1>
       <p>Ti aiutiamo a far emergere le tue qualità, costruendo insieme la strategia più adatta per raggiungere i tuoi obbiettivi.</p>
       </div></div>
       </div>
       </div>
   </section>
    )
  }
}
export default Servizi