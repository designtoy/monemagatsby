import React, { Component } from 'react'
import ContattiImg from '../../assets/images/contatti.png'
import SimpleMap from '../map'
import FontAwesome from 'react-fontawesome'
class Contatti extends Component {
  render() {
    return (
      <section id="contatti">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-header">
                <h1>Contattaci!</h1>
                
                <img src={ContattiImg} alt="" />
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="mapbox">
              <SimpleMap />
              <div className="text">
                <p>
                  <FontAwesome
                    name="map-marker"
                    size="2x"
                    className="icona"
                    style={{
                      color: 'black',
                      display: 'block',
                      fontSize: '1.5rem',
                    }}
                  />
                  <span>Sede:</span>
                  Via Valentini 14, Prato, ITALIA
                </p>
                <p>
                <FontAwesome
                    name="phone"
                    size="2x"
                    className="icona"
                    style={{
                      color: 'black',
                      fontSize: '1.3rem',
                      margin:'1rem'
                    }}
                  /> 
                  <span> telefono:</span> +39 0574 870600 |  
                  <FontAwesome
                    name="paper-plane"
                    size="2x"
                    className="icona"
                    style={{
                      color: 'black',
                      fontSize: '1.3rem',
                      margin:'1rem'
                    }}
                  />  
                  <span>email:</span>{' '}
                 <a href="mailto:info@monema.it">info@monema.it</a> 
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Contatti
