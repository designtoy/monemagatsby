import React, { Component, Fragment } from 'react'
import LavoriImg from '../../assets/images/compiti.png'
import DescrizioneProg from './descrizioneProg'
import {
  Transition,
  TransitionGroup,
  CSSTransition,
} from 'react-transition-group'
import ReactTimeout from 'react-timeout'
import './lavori.scss'




class Lavori extends Component {
  constructor(props) {
    super(props)
    this.state = {
      workSelected: this.props.lavoriData,
      toggleList: false,
      classSingleProgetto: 'col-lg-4',
      singleWork: this.props.lavoriData,
    }
  }

  resetList = () => {
    this.setState({
      toggleList: false,
    });
    window.location.href = '#' + 'lavori'
  }

  selectFromWorks = indexWork => {
    this.setState(
      {
        toggleList: !this.state.toggleList,
      },
      () => {
        if (this.state.toggleList === true) {
          const newArrayWork = this.state.workSelected.filter(function(
            lavoro,
            index
          ) {
            return index === indexWork
          })
          this.setState({
            singleWork: newArrayWork,
          })
          
        }
      }
    )
  }

  render() {
    return (
      <section id="lavori" className="container">
        <div className="row">
          <div className="section-header" data-aos="fade-right">
            <h1>Lavori</h1>
            <h2>Il nostro portfolio...</h2>
            <img src={LavoriImg} alt="" />
          </div>
        </div>

        <div className="row">
   
            {this.state.toggleList === false ? (
              /* component=null per non renderizzare un div  */
              <TransitionGroup component={null}>
                <Transition
                  key="0"
                  timeout={800}
                  in={this.state.toggleList}
                  appear
                >
                  {style => (
                    <Fragment>
                      {this.state.workSelected.map((progetto, index) => (
                        <div  onClick={() => {
                          this.selectFromWorks(index)
                        }} key={index}className={'col-lg-4 ' + style}>
                          <div
                            className="thumbnail"
                           
                          >
                            <div className="caption">
                              <h1>{progetto.titolo}</h1>
                            </div>

                            <img
                              src={progetto.image.childImageSharp.sizes.src}
                              alt="..."
                            />
                          </div>
                        </div>
                      ))}
                    </Fragment>
                  )}
                </Transition>
              </TransitionGroup>
            ) : (
              <TransitionGroup component={null} appear>
                <CSSTransition
                  key="1"
                  timeout={400}
                  in={this.state.toggleList}
                  classNames="message"

                >
                  <DescrizioneProg
                    progetto={this.state.singleWork}
                    resetList={this.resetList}
                  />
                </CSSTransition>
              </TransitionGroup>
            )}
      
        </div>
      </section>
    )
  }
}
export default ReactTimeout(Lavori)
