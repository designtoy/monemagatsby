import React, { Fragment } from 'react'

const DescrizioneProg = props => {
  return (
    <Fragment>
      {props.progetto.map(progetto => {
        return (
          <div key="progetto.titolo" className='descwrapper'>
          <div className='single'>
          <h1 className="backbtn" onClick={props.resetList}>back</h1>
          <img src={progetto.modalimg.childImageSharp.sizes.src} alt="" />
           <div className="descwrap">
           <h1>{progetto.titolo}</h1>
           <div className="progdesc">{progetto.descrizione}
          
           </div>
           <div className="more">...More
           <div className='single-hover'>
           <a href={progetto.url} ><h1>Website</h1></a>
           </div>
           </div>
           </div> 

          </div>
          </div>
        )
      })}
    </Fragment>
  )
}
export default DescrizioneProg
