import React, { Component } from 'react'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
} from 'reactstrap'
import {Link} from 'gatsby'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import SocialTop from './socialtop/socialTop'
import LogoImg from '../assets/images/logo.png'
import FontAwesome from 'react-fontawesome'

class NavBar extends Component {
  constructor (props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false

    }
  }
  toggle () {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }
  render () {
    return (
      <div>

        <Navbar color="light" light expand="md">

          <NavbarBrand href="/"><img src={LogoImg} alt="Logo" /></NavbarBrand>

          <NavbarToggler onClick={this.toggle} />

          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <AnchorLink className="nav-link" offset='100' href="#lavori">Lavori</AnchorLink>
              </NavItem>
              <NavItem>
                <AnchorLink className="nav-link" offset='100' href="#servizi">Servizi</AnchorLink>
              </NavItem>
              <NavItem>
                <AnchorLink className="nav-link" offset='100' href="#contatti">Contatti</AnchorLink>
              </NavItem>
              <NavItem>
                <Link className="nav-link"  to='/blog/'>BLOG</Link>
              </NavItem>
              <NavItem>
                <a href="#" onClick={this.props.handler}>
                  <FontAwesome
                    name="user-md"
                    className="icona-supporto"
                    style={{ color: 'rgb(73, 73, 73)' }}
                  /></a>
              </NavItem>
            </Nav>
          </Collapse>

        </Navbar>
        <SocialTop />
      </div>
    )
  }
}

export default NavBar
