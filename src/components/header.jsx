import React from 'react'
import FontAwesome from 'react-fontawesome'
import Img from "gatsby-image"
import Slider from "react-slick"
import NavBar from './navbar'
import AnchorLink from 'react-anchor-link-smooth-scroll'

var settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true
};
const Header = (props) => (
  <div>
    <header className="container">
      <nav>
        <div className="row">
          <div className="col-md-12">
            <NavBar handler={props.handler} />
          </div>
        </div>
      </nav>
    </header>

    <section id="slider" className="container-fluid">

      <Slider {...settings}>
        {props.sliderData.map((slide, index) => (

          <div key={index}>
            <Img
              title="Header image"
              alt="Greek food laid out on table"
              position='absolute'
              sizes={slide.image.childImageSharp.sizes}
              className='innerSlide'
              outerWrapperClassName='outerSlide'
            />
            <div className="textslide">
              <h1>{slide.titolo}</h1>
              <p>{slide.pay}</p>
              <p>

                <AnchorLink className="nav-link" offset='100' href='#servizi'>
                  <FontAwesome
                    name="chevron-circle-down"
                    className="icona"
                    style={{ color: '#259ccb' }}
                  /></AnchorLink >

              </p>
            </div>
          </div>
        ))}
      </Slider>
    </section>
  </div>
)

export default Header
