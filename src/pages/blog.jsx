import React from 'react'

import Layout from '../components/layout/layout';
import { graphql } from 'gatsby'

const blog = ({data}) => {
  const frontmatter = data.allMarkdownRemark.edges
  console.log(frontmatter)
return(
  <Layout>
  {frontmatter.map((frontmatter)=>{
return(
  <div>
  <h1>{frontmatter.node.frontmatter.title}</h1>
  <p dangerouslySetInnerHTML={{ __html: frontmatter.node.excerpt }}/>
  </div>
)

  })}
  </Layout>
)
}

export const blogroll = graphql`
 query {

  allMarkdownRemark{
    edges{
      node{
        frontmatter{
          title
          thumbnail
        }
        html
        excerpt(pruneLength: 10)
      }
    }
  
}
 }
`
export default blog