import React, { Component } from 'react'
import Lavori from '../components/lavori/lavori'
import Servizi from '../components/servizi/servizi'
import Divider from '../components/divider'
import Contatti from '../components/contatti/contatti'
import Footer from '../components/footer/footer'
import ScrollToTop from 'react-scroll-up'
import { graphql } from 'gatsby'
import Layout from '../components/layout/layout'
import Header from '../components/header'
import Helmet from 'react-helmet'
import FontAwesome from 'react-fontawesome'
export default class Index extends Component {
  
  constructor(props) {
    super(props)
    this.toggler = this.toggler.bind(this)

    this.state = {
      isopen: false,
    }
  }

  toggler(e) {
    e.preventDefault()
    this.setState({
      isopen: !this.state.isopen,
    })
  }
  /**/
  
  /* Libreria di AOS */
componentDidMount(){
  const AOS = require('aos');
  const aos = AOS
  aos.init({
    duration: 2000
  });
}


  render() {
    return (
      <>
        <Layout>
          <div className={`supporto ${this.state.isopen ? 'isopen' : ''}`}>
            <div className="container">
              <div className="row">
                <div className="col-lg-8 offset-lg-2">
                  <a
                    href="#"
                    style={{
                      width: '100%',
                      display: 'block',
                      marginTop: 50,
                      height: 50,
                      textAlign: 'center',
                      lineHeight: 3.6,
                    }}
                    onClick={this.toggler}
                  >
                    <FontAwesome
                      onClick={this.toggler}
                      name="close"
                      className="icona-supporto"
                      style={{ color: 'rgb(73, 73, 73)' }}
                      size="2x"
                    />
                  </a>
                  <div className="boxsupporto">
                    <h1>supporto</h1>
                  </div>
                  <form className="form">
                    <label className="my-1 mr-2">Preference</label>
                    <select
                      className="custom-select my-1 mr-sm-2"
                      id="inlineFormCustomSelectPref"
                    >
                      <option value="selected">Choose...</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>

                    <div className="form-group">
                      <label>Example textarea</label>
                      <textarea
                        className="form-control"
                        id="exampleFormControlTextarea1"
                        rows="3"
                      />
                    </div>
                    <button type="submit" className="btn btn-primary my-1">
                      Submit
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <Helmet
            title={this.props.data.site.siteMetadata.title}
            meta={[
              { name: 'description', content: 'Sample' },
              { name: 'keywords', content: 'sample, something' },
            ]}
          />
          <Header
            handler={this.toggler}
            sliderData={this.props.data.allSliderYaml.edges[0].node.slider}
            siteTitle={this.props.data.site.siteMetadata.title}
            style={{ opacity: 0 }}
          />
          <Divider />
          <Lavori
            lavoriData={this.props.data.allPortfolioYaml.edges[0].node.lavori}
          />
          <Divider />
          <Servizi />
          <Divider />
          <Contatti />
          <Footer />
          <ScrollToTop showUnder={160}>
            <span className="upscroll">UP</span>
          </ScrollToTop>
        </Layout>
      </>
    )
  }
}

export const query = graphql`
  query main {
    allPortfolioYaml {
      edges {
        node {
          lavori {
            modalimg {
              childImageSharp {
                sizes(maxWidth: 1000, maxHeight: 750) {
                  ...GatsbyImageSharpSizes
                  src
                  srcSet
                }
              }
            }
            image {
              childImageSharp {
                sizes(maxWidth: 600, maxHeight: 600) {
                  ...GatsbyImageSharpSizes
                  src
                }
              }
            }
            titolo
            tipo
            descrizione
            url
          }
        }
      }
    }
    site {
      siteMetadata {
        title
      }
    }

    allSliderYaml {
      edges {
        node {
          slider {
            titolo
            pay
            image {
              childImageSharp {
                sizes(maxWidth: 1904) {
                  ...GatsbyImageSharpSizes
                  src
                  srcSet
                }
              }
            }
          }
        }
      }
    }
  }
`
