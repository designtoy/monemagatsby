module.exports = {
  siteMetadata: {
    title: 'Monema Website',
  },
  plugins: [
    

    'gatsby-plugin-react-helmet',
    `gatsby-transformer-yaml`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-sharp`,
    'gatsby-transformer-sharp',
    `gatsby-remark-images`,
    `gatsby-plugin-netlify-cms`,

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/data/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'img',
        path: `${__dirname}/src/assets/images/`,
      },
    },
    //
    // !!! alberatura per utilizzare le immagini nel markdown !!!
    //

    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 590,
            },
          },
        ],
      },
    },
   {
    resolve: `gatsby-source-filesystem`,
    options: {
      path: `${__dirname}/src/blog/`,
      name: "markdown-pages",
    },
  },
  `gatsby-transformer-remark`,  
  ],
}
